import 'bootstrap/dist/css/bootstrap.css'
import '../styles/globals.css'
import '../styles/custom_style.css'
import Header from '../components/header'
import Footer from '../components/footer'
import '../styles/home-one.css'

import "regenerator-runtime/runtime"
import { ThirdwebWeb3Provider } from "@3rdweb/hooks";
const supportedChainIds = [1, 4, 137];
const connectors = {
	injected: {

	}
}

import "regenerator-runtime/runtime";


function MyApp({ Component, pageProps }) {

  return (
	<ThirdwebWeb3Provider
	supportedChainIds={supportedChainIds}
	connectors={connectors}
	>
		<Header/>
		<Component {...pageProps} />
		<Footer />
	</ThirdwebWeb3Provider>

  )
}

export default MyApp
