import React, { useState } from "react";
import { useWeb3 } from "@3rdweb/hooks";
const supportedChainIds = [1, 4, 137];

export default function Login() {
  const { address, chainId, connectWallet, disconnectWallet } = useWeb3();

  if (address) {
    return (
      <div className="loggedwrap">
        <p className="walletid">Wallet ID: {address}</p>
        <p>ChainId: {chainId}</p>
        {address && (
          <button className="btn btn-success mr-2" onClick={disconnectWallet}>
            Disconnect
          </button>
        )}

        {/* <button className="btn btn-primary mr-2" onClick={() => connectWallet("walletlink")}>Connect Coinbase Wallet</button>
                <button className="btn btn-primary mr-2" onClick={() => connectWallet("walletconnect")}>Connect Wallet Connect</button>
                <button className="btn btn-primary" onClick={() => connectWallet("walletlink")}>Connect Coinbase Wallet</button> */}
      </div>
    );
  } else {
    return (
      <div className="content-wrap clearfix">
        <div className="container">
          <div className="loginwrap">
            <div className="loginlogo">
              <img
                src="https://via.placeholder.com/150x150/C4C4C4/000000"
                alt=""
              />
            </div>
            <h1 className="login-title">Create NFT Port Landing Page</h1>
            <div className="login-btn">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => connectWallet("injected")}
              >
                Connect Wallet
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
