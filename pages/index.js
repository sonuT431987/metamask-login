
import Image from 'next/image'

export default function Home() {
  return (
		<>
			<div className="container-fluid">
				<div className="user-wraps">
					<div className="user-wrap">
						<Image src="/images/profile-bg.png" width={1903} height={452} alt="" />
						<span className="editimg">
							<Image src="/images/camera.svg" width={30} height={31} alt="" />
							<input className="browse photo" type="file" name="" />
							<Image src="/images/editicons.svg" width={30} height={31} alt="" />
						</span>
						<div className="user-img">
							<div className="userimg">
								<Image id="imgPreview" className="previewimg" src="/https://via.placeholder.com/250x250/FFFFFF" width={250} height={250} alt="" />
								<span className="editimg">
									<input id="photo" className="browse photo" type="file" name="" />
									<Image src="/images/editicons.svg" width={30} height={31} alt="" />
								</span>
							</div>
						</div>
					</div>
					<div className="bgwraps">
						<div className="user-info">
							<h2 className="user-name">@Username</h2>
							<div className="user-copy">0xceB945...Bd3c8D5D <i className="fa fa-clone" aria-hidden="true"></i></div>
							<div className="user-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</div>
							<div className="social-list">
								<a href="#"><i className="fab fa-twitter" aria-hidden="true"></i></a>
								<a href="#"><i className="fab fa-instagram" aria-hidden="true"></i></a>
								<a href="#"><i className="fab fa-facebook-square" aria-hidden="true"></i></a>
							</div>
							<div className="btn-groups">
								<button type="button" className="btn btn-default">NFTs</button>
								<button type="button" className="btn btn-default">Collections</button>
								<button type="button" className="btn btn-default">Exhibitions</button>
							</div>
						</div>
						<span className="editimg">
							<Image src="/images/camera.svg" width={30} height={31} alt="" />
							<input className="browse photo" type="file" name="" />
							<Image src="/images/editicons.svg" width={30} height={31} alt="" />
						</span>
					</div>
				</div>
				<div className="grid-wrap grid-3">
					<div className="grid-col"><Image src="/https://via.placeholder.com/600x324/C4C4C4/000000" width={630} height={324} alt="" /></div>
					<div className="grid-col"><Image src="/https://via.placeholder.com/600x324/C4C4C4/000000" width={630} height={324} alt="" /></div>
					<div className="grid-col"><Image src="/https://via.placeholder.com/600x324/C4C4C4/000000" width={630} height={324} alt="" /></div>
					<div className="grid-col"><Image src="/https://via.placeholder.com/600x324/C4C4C4/000000" width={630} height={324} alt="" /></div>
					<div className="grid-col"><Image src="/https://via.placeholder.com/600x324/C4C4C4/000000" width={630} height={324} alt="" /></div>
					<div className="grid-col"><Image src="/https://via.placeholder.com/600x324/C4C4C4/000000" width={630} height={324} alt="" /></div>
				</div>
			</div>
			<div className="container-fluid">
				<div className="user-wraps">
					<div className="user-wrap">
					<Image src="/images/profile-bg.png" width={1903} height={452} alt="" />
						<span className="editimg">
							<Image src="/images/camera.svg" width={30} height={31} alt="" />
							<input className="browse photo" type="file" name="" />
							<Image src="/images/editicons.svg" width={30} height={31} alt="" />
						</span>
						<div className="user-img">
							<div className="userimg">
								<Image id="imgPreview" className="previewimg" src="/https://via.placeholder.com/250x250/FFFFFF" width={250} height={250} alt="" />
								<span className="editimg">
									<input id="photo" className="browse photo" type="file" name="" />
									<Image src="/images/editicons.svg" width={30} height={31} alt="" />
								</span>
							</div>
						</div>
					</div>
					<div className="bgwraps">
						<div className="user-info">
							<h2 className="user-name">@Username</h2>
							<div className="user-copy">0xceB945...Bd3c8D5D <i className="fa fa-clone" aria-hidden="true"></i></div>
							<div className="user-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</div>
							<div className="social-list">
								<a href="#"><i className="fab fa-twitter" aria-hidden="true"></i></a>
								<a href="#"><i className="fab fa-instagram" aria-hidden="true"></i></a>
								<a href="#"><i className="fab fa-facebook-square" aria-hidden="true"></i></a>
							</div>
							<div className="btn-groups">
								<button type="button" className="btn btn-default">NFTs</button>
								<button type="button" className="btn btn-default">Collections</button>
								<button type="button" className="btn btn-default">Exhibitions</button>
							</div>
						</div>
						<span className="editimg">
							<Image src="/images/camera.svg" width={30} height={31} alt="" />
							<input className="browse photo" type="file" name="" />
							<Image src="/images/editicons.svg" width={30} height={31} alt="" />
						</span>
					</div>
				</div>
				{/* <div className="grid">
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/animals" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/300/arch" width={640} height={300} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/nature" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/500/people" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/700/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/arch" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/640/nature" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/800/animals" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/700/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/arch" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/640/nature" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/800/animals" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/700/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/arch" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/640/nature" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/800/animals" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/700/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/arch" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/640/nature" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/800/animals" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/700/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/arch" width={640} height={480} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/480/tech" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/640/nature" width={} height={} alt="" /></div>
					<div className="grid-item"><Image src="/https://placeimg.com/640/800/animals" width={} height={} alt="" /></div>
				</div> */}
			</div>		
		</>
	)
}
