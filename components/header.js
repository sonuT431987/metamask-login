import React from "react";
import Link from "next/link";
import { useWeb3 } from "@3rdweb/hooks";

const Header = () => {
  const { address, connectWallet, disconnectWallet } = useWeb3();
  return (
    <header id="header-set-1128" className="header-set-1128">
      <div className="container-fluid">
        <div className="header-row">
          <div className="col-left">
            <div className="logo">
              <img
                src="https://via.placeholder.com/150x50/C4C4C4/000000"
                alt=""
              />
            </div>
            <nav className="menu">
              <ul>
                <li>
                  <Link href="/">
                    <a>My Profile</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>Submit NFT</a>
                  </Link>
                </li>
                <li>
                  <Link href="/">
                    <a>Theme</a>
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
          <div className="wallet-wrap">
            <ul>
              <li>
                <Link href="/">
                  {address ? (
                    <span className="mataaddress">Hello, {address}</span>
                  ) : (
                    <Link href="/">
                      <a onClick={() => connectWallet("injected")} className="">
                        Connect Wallet
                      </a>
                    </Link>
                  )}
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a onClick={() => disconnectWallet("injected")}>Disconnect</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
